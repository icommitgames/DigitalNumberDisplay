using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DigitalNumberDisplay : MonoBehaviour
{
  List<bool[]> IndicatorIndex = new List<bool[]>
  {
    new bool[]{true,true,true,false,true,true,true},new bool[]{false,false,true,false,false,true,false},
    new bool[]{true,false,true,true,true,false,true},new bool[]{true,false,true,true,false,true,true},
    new bool[]{false,true,true,true,false,true,false},new bool[]{true,true,false,true,false,true,true},
    new bool[]{true,true,false,true,true,true,true},new bool[]{true,false,true,false,false,true,false},
    new bool[]{true,true,true,true,true,true,true},new bool[]{true,true,true,true,false,true,true},
    new bool[]{false,false,false,true,false,false,false}
  };
  List<GameObject> PreDecimalDigitObjects = new List<GameObject>();
  List<GameObject> PostDecimalDigitObjects = new List<GameObject>();
  GameObject DecimalPoint;

  public Color activeIndicatorColor = new Color(1f,0f,0f,1f);
  public Color inactiveIndicatorColor = new Color(0f,0f,0f,0.15f);
  public Color backgroundColor = new Color(0f,0f,0f,0.5f);
  void Start()
  {
    PreDecimalDigitObjects.Add(gameObject.transform.GetChild(0).gameObject);
    gameObject.transform.GetChild(0).gameObject.SetActive(false);
    DecimalPoint = gameObject.transform.GetChild(1).gameObject;
    DecimalPoint.SetActive(false);
  }
  public void DisplayNumber(float input, int decimalPlaces, int leadingZeros)
  {
    decimalPlaces = Mathf.Clamp(decimalPlaces, 0, 5);
    SetDisplay(Convert.ToDouble(input), decimalPlaces, leadingZeros);
  }
  public void DisplayNumber(double input, int decimalPlaces, int leadingZeros)
  {
    decimalPlaces = Mathf.Clamp(decimalPlaces, 0, 10);
    SetDisplay(input, decimalPlaces, leadingZeros);
  }
  void SetDisplay(double input, int decimalPlaces, int leadingZeros)
  {
    bool negative = (Math.Sign(input) < 0) ? true : false;
    List<int>[] DigitsToDisplay = GetDigits(Math.Abs(input), (decimalPlaces == 0) ? true : false);
    SetAllDigits(DigitsToDisplay[0], DigitsToDisplay[1], decimalPlaces, leadingZeros, negative);
  }
  List<int>[] GetDigits(double input, bool round)
  {
    List<int> PreDecimalDigits = new List<int>();
    int preDecimalRemainder = (round) ? Mathf.RoundToInt((float)input) : Mathf.RoundToInt((float)Math.Floor(input));
    while(preDecimalRemainder > 0)
    {
      int digit = preDecimalRemainder % 10;
      preDecimalRemainder -= digit;
      preDecimalRemainder = preDecimalRemainder / 10;
      PreDecimalDigits.Add(digit);
    }
    List<int> PostDecimalDigits = new List<int>();
    long postDecimalRemainder = Convert.ToInt64((input - Math.Floor(input)) * 10000000000);
    while(postDecimalRemainder > 0)
    {
      int digit = Convert.ToInt32(postDecimalRemainder % 10);
      postDecimalRemainder -= digit;
      postDecimalRemainder = postDecimalRemainder / 10;
      PostDecimalDigits.Insert(0, digit);
    }
    return new List<int>[]{PreDecimalDigits, PostDecimalDigits};
  }
  void SetAllDigits(List<int> PreDecimalDigits, List<int> PostDecimalDigits, int decimalPlaces, int leadingZeros, bool negative)
  {
    float digitWidth = PreDecimalDigitObjects[0].GetComponent<RectTransform>().sizeDelta.x;
    float pointWidth = DecimalPoint.GetComponent<RectTransform>().sizeDelta.x;
    DecimalPoint.SetActive((decimalPlaces > 0) ? true : false);
    DecimalPoint.GetComponent<Image>().color = backgroundColor;
    DecimalPoint.transform.GetChild(0).GetComponent<Image>().color = activeIndicatorColor;
    float preDecimalStart = (decimalPlaces > 0) ? (pointWidth / 2f) + (digitWidth / 2f) : 0f;
    Vector3 position = new Vector3(-preDecimalStart,0f,0f);
    int index = 0;
    for(int i = 0; i < PreDecimalDigits.Count || i < leadingZeros; i++)
    {
      index++;
      if(PreDecimalDigitObjects.Count <= i)
      {
        GameObject CreatedDigit = Instantiate(PreDecimalDigitObjects[0], gameObject.transform);
        PreDecimalDigitObjects.Add(CreatedDigit);
      }
      else
      {
        PreDecimalDigitObjects[i].SetActive(true);
      }
      PreDecimalDigitObjects[i].GetComponent<Image>().color = backgroundColor;
      int numberToSet = (i < PreDecimalDigits.Count) ? PreDecimalDigits[i] : 0;
      SetDigit(numberToSet, PreDecimalDigitObjects[i]);
      PreDecimalDigitObjects[i].transform.localPosition = position;
      position -= new Vector3(digitWidth,0f,0f);
    }
    if(negative)
    {
      if(PreDecimalDigitObjects.Count <= index)
      {
        GameObject CreatedDigit = Instantiate(PreDecimalDigitObjects[0], gameObject.transform);
        PreDecimalDigitObjects.Add(CreatedDigit);
      }
      else
      {
        PreDecimalDigitObjects[index].SetActive(true);
      }
      PreDecimalDigitObjects[index].GetComponent<Image>().color = backgroundColor;
      SetDigit(10, PreDecimalDigitObjects[index]);
      PreDecimalDigitObjects[index].transform.localPosition = position;
      index++;
    }
    for(int i = index; i < PreDecimalDigitObjects.Count; i++)
    {
      PreDecimalDigitObjects[i].SetActive(false);
    }
    position = new Vector3((pointWidth / 2f) + (digitWidth / 2f),0f,0f);
    for(int i = 0; i < decimalPlaces; i++)
    {
      if(PostDecimalDigitObjects.Count <= i)
      {
        GameObject CreatedDigit = Instantiate(PreDecimalDigitObjects[0], gameObject.transform);
        PostDecimalDigitObjects.Add(CreatedDigit);
      }
      else
      {
        PostDecimalDigitObjects[i].SetActive(true);
      }
      PostDecimalDigitObjects[i].GetComponent<Image>().color = backgroundColor;
      int numberToSet = (i < PostDecimalDigits.Count) ? PostDecimalDigits[i] : 0;
      SetDigit(numberToSet, PostDecimalDigitObjects[i]);
      PostDecimalDigitObjects[i].transform.localPosition = position;
      position += new Vector3(digitWidth,0f,0f);
    }
    for(int i = decimalPlaces; i < PostDecimalDigitObjects.Count; i++)
    {
      PostDecimalDigitObjects[i].SetActive(false);
    }
  }
  void SetDigit(int number, GameObject DigitToSet)
  {
    for(int i = 0; i < 7; i++)
    {
      bool active = IndicatorIndex[number][i];
      DigitToSet.transform.GetChild(i).GetComponent<Image>().color = (active) ? activeIndicatorColor : inactiveIndicatorColor;
    }
  }
}
