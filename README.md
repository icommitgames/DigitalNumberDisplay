A digital number display for Unity able to handle 32 and 64-bit floats<br>
<br>
Released under a CC0 (public domain) license<br>
Explainer: https://creativecommons.org/share-your-work/public-domain/cc0/<br>
TLDR: This project can be used freely and without any restrictions<br>
<br>
Install directions:<br>
Clone into Assets or subfolder and call<br>
DisplayNumber(float numberToDisplay, int decimalPlacesToDisplay, int leadingZerosToDisplay)<br>
or<br>
DisplayNumber(double numberToDisplay, int decimalPlacesToDisplay, int leadingZerosToDisplay)<br>